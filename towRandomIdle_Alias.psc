Scriptname towRandomIdle_Alias extends ReferenceAlias  

Import towRandomIdle

Idle Property  IdleStop_Loose Auto

ObjectReference SelfRef
bool bDebugMessage = false
int ShoutKey
bool bRunIdle

Event OnInit()
	Init()
EndEvent

Event OnPlayerLoadGame()
	Init()
endEvent

Function Init()
	SelfRef = Self.GetReference()
	if (SelfRef == Game.GetPlayer())
		ShoutKey = Input.GetMappedKey("Shout")
		RegisterForKey(ShoutKey)
	endif

	RegisterForAnimationEvent(SelfRef,  "tailMTIdle")
	RegisterForAnimEvent(SelfRef,  "moveStart")
	RegisterForAnimEvent(SelfRef,  "SneakStart")
	RegisterForBypassEvent(SelfRef,  "Magic_Equip")
	RegisterForBypassEvent(SelfRef,  "WeapEquip")
endFunction

Event OnBypassEvent(ObjectReference akSource, string asEventName)
	if (bRunIdle)
		Debug.SendAnimationEvent(akSource, "JumpLandEnd")
	endif
	SwitchBypassEvent(False)
	Debug.SendAnimationEvent(akSource, asEventName)
	SwitchBypassEvent(True)
endEvent

Event OnKeyDown(Int KeyCode)
	if (SelfRef == Game.GetPlayer())
 		if (KeyCode == ShoutKey)
			OnAnimationEventEX(SelfRef, "KeydownEvent")
		endif
	endif
EndEvent

Event OnAnimationEvent(ObjectReference akSource, string asEventName)
	if (akSource == SelfRef)
		if (asEventName == "tailMTIdle")
			OnAnimationEventEX(akSource, asEventName)
		endif
	endif
endEvent

Event OnAnimationEventEX(ObjectReference akSource, string asEventName)
	if (akSource == SelfRef)
		if (bRunIdle)
			if (asEventName == "moveStart")
				Debug.SendAnimationEvent(akSource, "JumpLandEnd")
				SwitchBypassEvent(false)
				bRunIdle = false
			elseif (asEventName == "SneakStart")
				Debug.SendAnimationEvent(akSource, "JumpLandEnd")
				SwitchBypassEvent(false)
				bRunIdle = false
			elseif (asEventName == "KeydownEvent")
				Debug.SendAnimationEvent(akSource, "JumpLandEnd")
				SwitchBypassEvent(false)
				bRunIdle = false
			endif
		endif
		
		if (asEventName == "tailMTIdle")
			string str = GetRandomIdleName(akSource)
			if (str != "")
; 				Debug.SendAnimationEvent(akSource, "JumpLandEnd")
				Debug.SendAnimationEvent(akSource, str)
			endif
		endif

		if (IsRandomIdleEvent(asEventName))
			SwitchBypassEvent(True)
			bRunIdle = true
		endif
	endif
endEvent

Event OnActorFurnitureActivate(ObjectReference akTarget, ObjectReference akSource)
	if (akSource == SelfRef)
		if (bRunIdle)
			Debug.SendAnimationEvent(akSource, "JumpLandEnd")
			bRunIdle = false
		endif
	endif
endEvent

String Function GetRandomIdleName(ObjectReference ref)
	if (ref)
		GlobalVariable gvMaxIdle = Game.GetFormFromFile(0x0D63, "towRandomIdle.esp") as GlobalVariable
		if (gvMaxIdle)
			int index = Utility.RandomInt(1, (gvMaxIdle.GetValue() as int))
			if (index)
				string str
				if (index < 10)
					str = "RandomIdle0" + index
				else
					str = "RandomIdle" + index
				endif
				return str
			endif
		endif
	endif
	return ""
endFunction
