Scriptname towRandomIdle hidden

; FUNCTIONS
bool Function IsRandomIdleEvent(String EventName) global native
Function SwitchBypassEvent(bool bSendEvent) global native
Function RegisterForAnimEvent_base(String EventName, Int Mode) global native

Function RegisterForBypassEvent(ObjectReference akRef, String EventName) global
	RegisterForAnimEvent_base(EventName, 2)
endFunction
Function RegisterForAnimEvent(ObjectReference akRef, String EventName) global
	RegisterForAnimEvent_base(EventName, 1)
endFunction
Function UnregisterForAnimEvent(ObjectReference akRef, String EventName) global
	RegisterForAnimEvent_base(EventName, 0)
endFunction
